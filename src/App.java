import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class App {
    WebDriver driver;
    public String badgeno01;
    public int varparameter = 1;
    public int VariableTime;
    public int vartime = 1500;
    String url = "https://sapm44.sp.zumtobel.co.at/manufacturing/com/sap/me/wpmf/client/template.jsf?WORKSTATION=Z_WORK_CENTER_TOUCH_STRESSTEST&THEME_NAME=TRADESHOW&ACTIVITY_ID=Z_TOUCH_POD_WC&sap-lsf-PreferredRendering=standards&WORKCENTER=MATBE&RESOURCE=MATBE&CONNECTION_ID=POD#";

    public App() {
		System.setProperty("webdriver.chrome.driver", "chromedriver_win32\\chromedriver.exe"); 
	}

    public static void main(String[] args) throws Exception {
        App apptest = new App();
        
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Which Programm do you want to start: \nClock in Clock out (1) \nStart and Finish SFCs (2)");
            String ProgramDecision = scanner.nextLine();
            int ProgramDecisionINT =Integer.parseInt(ProgramDecision);
            switch(ProgramDecisionINT){
                case 1:
                    System.out.println("Clock in Clock out is started");
                    apptest.ClockInClockOut();
                    break;
                case 2:
                    System.out.println("Start and Finish SFCs is started");
                    apptest.SFCStartandFinish();
                    break;
                default:
                    System.out.println("You have to enter (1) , (2)");
                    break;
                }
        }

    }

    private void tearDown() {
		System.out.println("End Time:" + java.time.LocalTime.now());
        wait5sec();
		driver.quit();
		System.out.println("Success: Shutdown of the Test-Automation");
	}
	
    private void wait05sec() {
		try{
			TimeUnit.MILLISECONDS.sleep(500);
		}catch (InterruptedException ie){
			System.out.println(ie.getMessage());
		}
		
	}

	private void wait1sec() {
		try{
			TimeUnit.SECONDS.sleep(1);
		}catch (InterruptedException ie){
			System.out.println(ie.getMessage());
		}
		
	}
	
	private void wait2sec() {
		try{
			TimeUnit.SECONDS.sleep(2);
		}catch (InterruptedException ie){
			System.out.println(ie.getMessage());
		}
		
	}
	
	private void wait5sec() {
		try{
			TimeUnit.SECONDS.sleep(5);
		}catch (InterruptedException ie){
			System.out.println(ie.getMessage());
		}
		
	}
	
	private void wait025sec() {
		try{
			TimeUnit.MILLISECONDS.sleep(250);
		}catch (InterruptedException ie){
			System.out.println(ie.getMessage());
		}
		
	}
    
    private void waitvartime() {
		try{
			int addtime = 0;
            int addtime01 = 250;
            
            for(int i=1;i<varparameter;i++){
                addtime = Integer.sum(addtime, addtime01);
                
            }
            if (addtime >= 1000){
                addtime = 1000;}

            TimeUnit.MILLISECONDS.sleep(Integer.sum(vartime, addtime));
            VariableTime = Integer.sum(addtime, addtime);
            
            
		}catch (InterruptedException ie){
			System.out.println(ie.getMessage());
		}
		
	}

    private void concircleLogin() throws IOException{ 
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Which Email should be used for LogIn:");
            String emailcon = scanner.nextLine(); 
            if (emailcon.equals("1")){
                emailcon = "ext.C.Neuwirth@zumtobelgroup.com";
            }

            driver.findElement(By.id("details-button")).click();
            driver.findElement(By.id("proceed-link")).click();
            wait2sec();
            WebElement EMAIL = driver.findElement(By.id("i0116"));
            //Dies ist keine sichere Verbindung
            EMAIL.sendKeys(emailcon);
            EMAIL.sendKeys(Keys.ENTER);
            driver.manage().window().maximize();
        
            System.out.println("Please press Enter to start the test run!");
            try {
                System.in.read();
            } catch (InterruptedIOException ie) {
                System.out.println(ie.getMessage());}
            wait2sec();
        }
    }

    private void SFCStartandFinish() throws IOException {
        
        
        try (//Parameters for Test run
        Scanner scanner = new Scanner(System.in)) {
            String site = "0002";
            String Quantity = "1";
            String Unit = "ST";
            String operationno = "";
            String sfcno = "";
            String shoporderno = "";
            String reportingno = "";
            String workcenterno = "";
            int operationstep = 1;
            int shoporderstep = 2;
            int sfcstep = 3;
            int reportingstep = 4;
            int workcenterstep = 5;
            int lineNo = 1;
            String BOMPOD = "";
            String PACKPOD = "";

            System.out.println("Which Material should be filled or is must-trace (eg. C03636743 / C154463 / C10034556):");
            String materialnumber = scanner.nextLine();
            if (materialnumber.equals("1")){
                materialnumber = "C03636743";
            }
            if (materialnumber.equals("2")){
                materialnumber = "C154463";
            }
            if (materialnumber.equals("3")){
                materialnumber = "C10034556";
            }
            System.out.println("Batch number (eg. SBatch1803):");
            String batchno = scanner.nextLine();
            if (batchno.equals("1")){
                batchno = "SBatch1803";
            }

            System.out.println("How often should it be scanned:");
            String materialquantity = scanner.nextLine();
            int materialquantityINT =Integer.parseInt(materialquantity);
                
            System.out.println("Log On Credentials for EWM URL (eg. NEUWIRTHC):");
            String LogOnCred = scanner.nextLine();
            if (LogOnCred.equals("1")){
                LogOnCred = "NEUWIRTHC";
            }
            System.out.println("How many SFC on one HU (eg. 20):");
            String HUQuantity = scanner.nextLine();
            int HUQuantityINT =Integer.parseInt(HUQuantity);
            
            String pathSFCList = "";
            System.out.println("Which SFC List do you want to take? (1 or 2 or 3):");
            String SFCFile = scanner.nextLine();
            if (SFCFile.equals("1")){
                pathSFCList = "SFCList_01.csv";
            }
            if (SFCFile.equals("2")){
                pathSFCList = "SFCList_02.csv";
            }
            if (SFCFile.equals("3")){
                pathSFCList = "SFCList_03.csv";
            }

            
            //String pathSFCList = "C:\\Users\\Clemens\\Documents\\Visual Studio Code Projects\\zt_Stresstest_withoutMaven\\SFCList_01.csv";
            File file= new File(pathSFCList);

            
            
            List<List<String>> lines = new ArrayList<>();
            Scanner inputStream;

            try{
                inputStream = new Scanner(file);

                while(inputStream.hasNext()){
                    String line= inputStream.next();
                    String[] values = line.split(";");
                    lines.add(Arrays.asList(values));
                }

                inputStream.close();
            }catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            for(List<String> line: lines) {
                
                for (String value: line) {
                     

                    if (lineNo == operationstep){
                        operationno = value;
                        operationstep = operationstep + 5;
                        }    
                    
                    else if (lineNo == shoporderstep){
                        shoporderno = value;
                        shoporderstep = shoporderstep + 5;
                        } 
                    
                    else if (lineNo == sfcstep){
                        sfcno = value;
                        sfcstep = sfcstep + 5;
                        }     
                    
                    else if (lineNo == reportingstep){
                        reportingno = value;
                        reportingstep = reportingstep + 5;
                        }          
                    
                    else if (lineNo == workcenterstep){
                        workcenterno = value;
                        workcenterstep = workcenterstep + 5;
                        }    
                        

                lineNo++;
                }

                String PackAppURL = "https://sapw74.sp.zumtobel.co.at/sap/bc/ui5_ui5/sap/zewm_packaging/index.html??SFC=&RESOURCE="+ workcenterno + "&LOGON=" + LogOnCred + "&SITE=" + site + "&ACTIVITY_ID=CON_EWM_PACK&WORKSTATION=Z_WORK_CENTER_TOUCH&WPMF_LEGACY_PLUGIN=true&POD_REQUEST=true&RESOURCE=" + workcenterno + "&LOGON="+ LogOnCred + "&SITE=" + site + "&ACTIVITY_ID=CON_EWM_PACK&WORKSTATION=Z_WORK_CENTER_TOUCH&POD_REQUEST=true&FORM_ID=DehOSJd0c47UlT_mynIvZgvyOnviPCHo&sap-language=de";               
                String BOMViewerURL = "https://sapm44.sp.zumtobel.co.at/XMII/CM/CON_EXTENSION/ASSEMBLY/index.html?SFC=" + sfcno + "&OPERATION=" + operationno + "&RESOURCE=" + workcenterno + "&SITE=" + site;
                
                if (lineNo == 6){
                    driver = new ChromeDriver();
                    driver.get(url);		
                    driver.manage().window().minimize();
                    wait2sec();
                    System.out.println("Success: Start of the Driver");
                    
                    concircleLogin();
                    
                    driver.switchTo().newWindow(WindowType.TAB);
                    wait1sec();
                    driver.get(BOMViewerURL);
                    wait5sec();
                    BOMPOD = driver.getWindowHandle();
                    
                    driver.switchTo().newWindow(WindowType.TAB);
                    wait1sec();
                    driver.get(PackAppURL);
                    wait5sec();
                    PACKPOD = driver.getWindowHandle();
                    driver.switchTo().window(BOMPOD);
                    wait1sec();
                }

                //BOM VIEWER
                JavascriptExecutor js = (JavascriptExecutor) driver;
                js.executeScript("$Ctr.barcodeReader.handleBarcodeScan('" + sfcno +"')");
                Instant start = Instant.now();
                
                WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
                WebElement Body = driver.findElement(By.xpath("/html/body"));
                String PSN = "PSN gestartet!";
                wait.until(ExpectedConditions.textToBePresentInElement(Body, PSN ));
                
                Instant end = Instant.now();
                Duration timeElapsed = Duration.between(start, end);
                System.out.println(java.time.LocalTime.now() + ": SFC Started! Time taken: "+ timeElapsed.toSeconds() +" Seconds");

                wait1sec();
                for(int i=1;i<=materialquantityINT;i++){
                    js.executeScript("$Ctr.barcodeReader.handleBarcodeScan('" + materialnumber + ";" + batchno + ";NSL K 3/3 +E-SCHR N-ERDE-L PC (Z);D202010;Q75');");
                    wait025sec();
                }
                wait1sec();
                
                

                //PACK VIEWER
                driver.switchTo().window(PACKPOD);
                WebElement packbody = driver.findElement(By.xpath("/html/body"));
                String HUtext = "Handling Unit";
                wait.until(ExpectedConditions.textToBePresentInElement(packbody, HUtext ));
                wait025sec();

                if ((lineNo - 6) % (HUQuantityINT * 5) == 0 || lineNo == 6) {
                    
               
                    WebElement OrderInput = driver.findElement(By.cssSelector("[id$=OrderInput-inner]"));
                    OrderInput.sendKeys(shoporderno);
                    OrderInput.sendKeys(Keys.ENTER);
                    wait025sec();
                    WebElement OperaInput = driver.findElement(By.cssSelector("[id$=OperationInput-inner]"));
                    OperaInput.sendKeys(reportingno);
                    OperaInput.sendKeys(Keys.ENTER);
                    wait025sec();
                    WebElement ResInput = driver.findElement(By.cssSelector("[id$=WorkCenterInput-inner]"));
                    ResInput.sendKeys(workcenterno);
                    wait025sec();
                    WebElement PackTypInput = driver.findElement(By.cssSelector("[id$=input0-inner]"));
                    PackTypInput.clear();
                    PackTypInput.sendKeys("9999");
                    PackTypInput.sendKeys(Keys.ENTER);
                    wait025sec();
                    WebElement PackMatInput = driver.findElement(By.cssSelector("[id$=PackagingMaterialInput-inner]"));
                    PackMatInput.clear();
                    PackMatInput.sendKeys("HUT9999");
                    PackMatInput.sendKeys(Keys.ENTER);
                    wait025sec();

                    driver.findElement(By.cssSelector("[id$=button0-inner]")).click();
                    wait2sec();

                    WebElement HUnumber = driver.findElement(By.cssSelector("[id$=HandlingUnitInput-inner]"));
                    System.out.println("Handling number: " + HUnumber.getAttribute("value") + " created!");

                }
                
                
                WebElement SFCScanInput = driver.findElement(By.cssSelector("[id$=SFCScanInput-inner]"));
                SFCScanInput.sendKeys(sfcno);
                SFCScanInput.sendKeys(Keys.ENTER);
                wait05sec();
                WebElement QuantityInput = driver.findElement(By.cssSelector("[id$=QuantityScanInput-inner]"));
                QuantityInput.sendKeys(Quantity);
                QuantityInput.sendKeys(Keys.ENTER);
                wait05sec();
                WebElement UnitInput = driver.findElement(By.cssSelector("[id$=UnitScanInput-inner]"));
                UnitInput.clear();
                UnitInput.sendKeys(Unit);
                UnitInput.sendKeys(Keys.ENTER);
                wait05sec();
                driver.findElement(By.cssSelector("[id$=button1-inner]")).click();
                wait05sec();
                wait.until(ExpectedConditions.textToBePresentInElement(packbody, sfcno ));
                wait025sec();

                
                if ((lineNo - 1) % (HUQuantityINT * 5) == 0) {
                    wait05sec();
                    driver.findElement(By.cssSelector("[id$=button4]")).click(); //static button
                    String text01 = "Bestätigung";
                    wait.until(ExpectedConditions.textToBePresentInElement(packbody, text01 ));
                    driver.findElement(By.xpath("//*[text()='OK']")).click();
                    String text02 = "Erfolg";
                    wait.until(ExpectedConditions.textToBePresentInElement(packbody, text02 ));
                    driver.findElement(By.xpath("//*[text()='OK']")).click();
                    wait1sec();
                    driver.findElement(By.cssSelector("[id$=button6]")).click(); //static button
                    wait.until(ExpectedConditions.textToBePresentInElement(packbody, text02 ));
                    driver.findElement(By.xpath("//*[text()='OK']")).click();
                    driver.navigate().refresh();
                    wait1sec();
                } 
                
                driver.switchTo().window(BOMPOD);
                String bompodtext = "Menge erforderlich: 1 PCE";;
                wait.until(ExpectedConditions.textToBePresentInElement(Body, bompodtext ));

            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        

    tearDown();
    }

    private void ClockInClockOut() throws IOException {

        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Workcenter/Ressource to test on (1 = MATBE , 2 = 46301, 3 = 55701):");
            String WrkCRss01 = scanner.nextLine();
            if (WrkCRss01.equals("1")){
                WrkCRss01 = "MATBE";
            }
            if (WrkCRss01.equals("2")){
                WrkCRss01 = "46301";
            }
            if (WrkCRss01.equals("3")){
                WrkCRss01 = "55701";
            }

            System.out.println("Badge Number to use on Workcenter (1 = Ogi , 2 = Segmen, 3 = Peter):");
            badgeno01 = scanner.nextLine();
            if (badgeno01.equals("1")){
                badgeno01 = "16098";
            }
            if (badgeno01.equals("2")){
                badgeno01 = "12571";
            }
            if (badgeno01.equals("3")){
                badgeno01 = "6360";
            }
            
            System.out.println("How long should the test run (in Minutes)?");
            String testruntime = scanner.nextLine();
            int testruntimeINT =Integer.parseInt(testruntime);
            int cycles = testruntimeINT * 6; 

            String urlClock = "https://sapm44.sp.zumtobel.co.at/manufacturing/com/sap/me/wpmf/client/template.jsf?WORKSTATION=Z_WORK_CENTER_TOUCH_STRESSTEST&THEME_NAME=TRADESHOW&ACTIVITY_ID=Z_TOUCH_POD_WC&sap-lsf-PreferredRendering=standards&WORKCENTER=" + WrkCRss01 + "&RESOURCE=" + WrkCRss01 + "#";

            //Start of the new Driver
            driver = new ChromeDriver();
            driver.get(urlClock);		
            driver.manage().window().minimize();
            wait2sec();
            System.out.println("Success: Start of the Driver");
            

            concircleLogin();
            

            driver.findElement(By.id("templateForm:pbtb:pbtbView:CLOCK_IN_OUT")).click();
            wait2sec();
            System.out.println("Start time:" + java.time.LocalTime.now());
                //calculation of rounds
                for(int i=1;i<=cycles;i++){


                    try{
                    ClockInClockOutButton();
                    ClockInClockOutBatchInput();
                    ClockInClockOutButton();  
                    ClockInClockOutBatchInput();
                    System.out.println("Round " + i + ": Successful");
                
                    }catch(Exception e){
                        System.out.println("Round " + i + ": Error");
                        System.out.println("Reload of the page necessary.");
                        varparameter ++;
                        System.out.println("The Testautomation is now " + VariableTime + "ms slower");
                        driver.navigate().refresh();
                        wait5sec();
                        driver.findElement(By.id("templateForm:pbtb:pbtbView:CLOCK_IN_OUT")).click();
                        wait1sec();
                    }
                    
                        
            }
        } catch (NumberFormatException e) {

            e.printStackTrace();
        }
        tearDown();
    }

    private void ClockInClockOutButton() throws IOException {
        driver.findElement(By.id("templateForm:PANEL_POPUP_WINDOW:clockInClockOutView:clockInOutBtn")).click();
        waitvartime();
        Boolean ClockInOutPopUp = driver.findElement(By.id("templateForm:dialogPopup1")).isDisplayed();
        if (ClockInOutPopUp = false){
            try{
                wait05sec();
                driver.findElement(By.id("templateForm:pbtb:pbtbView:CLOCK_IN_OUT")).click();
                wait1sec();
            }catch(Exception f){
                WebElement badgeNr = driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1:userLoginView:badgeField"));
                badgeNr.sendKeys(Keys.ENTER);
                wait1sec();
            } finally {
                if (ClockInOutPopUp = false){
                    System.out.println("Error in Finally Block");
                }
            }
        }
    }
    
    private void ClockInClockOutBatchInput() throws IOException {
        Boolean ClockInOutPopUp = driver.findElement(By.id("templateForm:dialogPopup1")).isDisplayed();
        if (ClockInOutPopUp = true){
        WebElement badgeNr = driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1:userLoginView:badgeField"));
        badgeNr.sendKeys(badgeno01);
        badgeNr.sendKeys(Keys.ENTER);
        waitvartime();
        }

        try{
        Boolean ClockInOutPopUp02 = driver.findElement(By.id("templateForm:dialogPopup1")).isDisplayed();
        if (ClockInOutPopUp02 = true){
            wait05sec();
            WebElement badgeNr = driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1:userLoginView:badgeField"));
            badgeNr.clear();
            badgeNr.sendKeys(badgeno01);
            badgeNr.sendKeys(Keys.ENTER);
            wait1sec();
            }   
        }catch(Exception f){
        }
    }
    // private void ClockInClockOut2Pods() throws IOException {
        
    //     try (//Parameters for Test run
    //     Scanner scanner = new Scanner(System.in)) {
    //         System.out.println("First Workcenter/Ressource to test on:");
    //         String WrkCRss01 = scanner.nextLine();

    //         System.out.println("Second Workcenter/Ressource to test on:");
    //         String WrkCRss02 = scanner.nextLine();

    //         System.out.println("First Badge Number to use on Workcenter:");
    //         String badgeno01 = scanner.nextLine();

    //         System.out.println("Second Badge Number to use on Workcenter:");
    //         String badgeno02 = scanner.nextLine();

    //         System.out.println("How long should the test run (in Minutes)?");
    //         String testruntime = scanner.nextLine();
    //         int testruntimeINT =Integer.parseInt(testruntime);
    //         int cycles = testruntimeINT * 6; 

    //         String urlClock01 = "https://sapm44.sp.zumtobel.co.at/manufacturing/com/sap/me/wpmf/client/template.jsf?WORKSTATION=Z_WORK_CENTER_TOUCH_STRESSTEST&THEME_NAME=TRADESHOW&ACTIVITY_ID=Z_TOUCH_POD_WC&sap-lsf-PreferredRendering=standards&WORKCENTER=" + WrkCRss01 + "&RESOURCE=" + WrkCRss01 + "&CONNECTION_ID=POD#";
    //         String urlClock02 = "https://sapm44.sp.zumtobel.co.at/manufacturing/com/sap/me/wpmf/client/template.jsf?WORKSTATION=Z_WORK_CENTER_TOUCH_STRESSTEST&THEME_NAME=TRADESHOW&ACTIVITY_ID=Z_TOUCH_POD_WC&sap-lsf-PreferredRendering=standards&WORKCENTER=" + WrkCRss02 + "&RESOURCE=" + WrkCRss02 + "&CONNECTION_ID=POD#";


    //         //Start of the new Driver
    //         driver = new ChromeDriver();
    //         driver.get(urlClock01);		
    //         driver.manage().window().minimize();
    //         wait2sec();
    //         System.out.println("Success: Start of the Driver!");

    //         concircleLogin();

    //         String PODUI1 = driver.getWindowHandle();

    //         driver.switchTo().newWindow(WindowType.TAB);
    //         wait1sec();
    //         driver.get(urlClock02);
    //         wait5sec();
    //         String PODUI2 = driver.getWindowHandle();
    //         wait05sec();

    //         driver.switchTo().window(PODUI1);
    //         wait05sec();
    //         driver.findElement(By.id("templateForm:pbtb:pbtbView:CLOCK_IN_OUT")).click();

    //         driver.switchTo().window(PODUI2);
    //         wait05sec();
    //         driver.findElement(By.id("templateForm:pbtb:pbtbView:CLOCK_IN_OUT")).click();

    //         driver.switchTo().window(PODUI1);
    //         wait05sec();


    //             //calculation of rounds
    //             for(int i=1;i<=cycles;i++){
                    
    //                 if(driver.getPageSource().contains("Provide the required value for User ID")) {
    //                     driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1_cancel_1")).click(); }
    //                 driver.findElement(By.id("templateForm:PANEL_POPUP_WINDOW:clockInClockOutView:clockInOutBtn")).click();
    //                 driver.switchTo().window(PODUI2);
    //                 wait1sec();
    //                 if(driver.getPageSource().contains("Provide the required value for User ID")) {
    //                     driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1_cancel_1")).click(); }
    //                 driver.findElement(By.id("templateForm:PANEL_POPUP_WINDOW:clockInClockOutView:clockInOutBtn")).click();
    //                 driver.switchTo().window(PODUI1);
    //                 wait1sec();
    //                 if(driver.getPageSource().contains("Provide the required value for User ID")) {
    //                     driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1_cancel_1")).click(); }
    //                 if(!driver.getPageSource().contains("Badge Number")) {
    //                     driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1_cancel_1")).click(); }

    //                 WebElement badgeNr01 = driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1:userLoginView:badgeField"));
    //                 badgeNr01.sendKeys(badgeno01);
    //                 badgeNr01.sendKeys(Keys.ENTER);
    //                 System.out.println(java.time.LocalTime.now());
    //                 wait1sec();  
    //                 if(driver.getPageSource().contains("you have clocked in")) {
    //                     System.out.println("Round "+ i +": Log In was successful");}
    //                     else{
    //                         System.out.println("Round "+ i +": ERROR: Log In was not successful");
    //                     }
                    
    //                 if(driver.getPageSource().contains("Provide the required value for User ID")) {
    //                     driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1_cancel_1")).click(); }
    //                 driver.findElement(By.id("templateForm:PANEL_POPUP_WINDOW:clockInClockOutView:clockInOutBtn")).click();
    //                 wait05sec();
    //                 if(driver.getPageSource().contains("Provide the required value for User ID")) {
    //                     driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1_cancel_1")).click(); }
    //                 if(!driver.getPageSource().contains("Badge Number")) {
    //                     driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1_cancel_1")).click(); }
    //                 driver.switchTo().window(PODUI2);
    //                 wait1sec();
    //                 if(driver.getPageSource().contains("Provide the required value for User ID")) {
    //                     driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1_cancel_1")).click(); }
    //                 if(!driver.getPageSource().contains("Badge Number")) {
    //                     driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1_cancel_1")).click(); }

    //                 WebElement badgeNr02 = driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1:userLoginView:badgeField"));
    //                 badgeNr02.sendKeys(badgeno02);
    //                 badgeNr02.sendKeys(Keys.ENTER);
    //                 System.out.println(java.time.LocalTime.now());
    //                 wait1sec();
    //                 if(driver.getPageSource().contains("you have clocked in")) {
    //                     System.out.println("Round "+ i +": Log In was successful");}
    //                     else{
    //                         System.out.println("Round "+ i +": ERROR: Log In was not successful");
    //                     }
                    
    //                 if(driver.getPageSource().contains("Provide the required value for User ID")) {
    //                     driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1_cancel_1")).click(); }
    //                 driver.findElement(By.id("templateForm:PANEL_POPUP_WINDOW:clockInClockOutView:clockInOutBtn")).click();
    //                 wait05sec();
    //                 if(driver.getPageSource().contains("Provide the required value for User ID")) {
    //                     driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1_cancel_1")).click(); }
    //                 if(!driver.getPageSource().contains("Badge Number")) {
    //                     driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1_cancel_1")).click(); }
    //                 driver.switchTo().window(PODUI1);
    //                 wait1sec();
    //                 if(driver.getPageSource().contains("Provide the required value for User ID")) {
    //                     driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1_cancel_1")).click(); }
    //                 if(!driver.getPageSource().contains("Badge Number")) {
    //                     driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1_cancel_1")).click(); }
                    

    //                 WebElement badgeNr03 = driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1:userLoginView:badgeField"));
    //                 badgeNr03.sendKeys(badgeno01);
    //                 badgeNr03.sendKeys(Keys.ENTER);
    //                 System.out.println(java.time.LocalTime.now());
    //                 wait1sec();  
    //                 if(driver.getPageSource().contains("you have clocked out")) {
    //                     System.out.println("Round "+ i +": Log Out was successful");}
    //                     else{
    //                         System.out.println("Round "+ i +": ERROR: Log OUT was not successful");
    //                     }
    //                 driver.switchTo().window(PODUI2);
    //                 wait1sec();

    //                 WebElement badgeNr04 = driver.findElement(By.id("templateForm:PANEL_POPUP_DIALOG1:userLoginView:badgeField"));
    //                 badgeNr04.sendKeys(badgeno02);
    //                 badgeNr04.sendKeys(Keys.ENTER);
    //                 System.out.println(java.time.LocalTime.now());  
    //                 wait05sec();
    //                 if(driver.getPageSource().contains("you have clocked out")) {
    //                     System.out.println("Round "+ i +": Log Out was successful");}
    //                     else{
    //                         System.out.println("Round "+ i +": ERROR: Log OUT was not successful");
    //                     }
    //                 driver.switchTo().window(PODUI1);
    //                 wait05sec();

    //                 System.out.println("Round " + i + ": Successful");
            
    //         }
    //     } catch (NumberFormatException e) {
    //         e.printStackTrace();
    //     }
    //     tearDown();
    // }

}















